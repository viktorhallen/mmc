
module Main where

numToDir 1 = "up"
numToDir 2 = "down"
numToDir 3 = "left"
numToDir 4 = "right"


show' Nothing = "No path found"
show'  (Just n) = [if a==' ' then '>' else a | a <- unwords $ map numToDir n] 

example = [
    ["user",    "empty",    "empty",    "empty",    "empty",    "empty"], 
    ["playlist","empty",    "empty",    "wall",     "wall",     "empty"], 
    ["wall",    "wall",     "wall",     "album",    "empty",    "empty"], 
    ["empty",   "monkey",   "empty",    "wall",     "empty",    "song"], 
    ["empty",   "wall",     "empty",    "empty",    "empty",    "wall"], 
    ["empty",   "wall",     "song",     "wall",     "empty",    "playlist"]]



allPaths :: Int -> Maybe [[String]] -> (Int,Int) -> Maybe [Int]
allPaths (-1)       _    _     = Nothing
allPaths _         Nothing _   = Nothing
allPaths numberLeft (Just map') (x,y) 
         | isDone map'               = Just []
         | otherwise                 = shortest $ map rec [1..4]
    where
    rec n = case getPos (x,y) n of
        (Just nPos) -> rec' n nPos
        _           -> Nothing
    rec' n nPos = case allPaths (numberLeft-1) (updateMap map' (x,y) n) nPos of
        (Just ans)  -> Just (n:ans)
        _           -> Nothing

shorterPath :: Int -> Maybe [[String]] -> (Int,Int) -> Maybe [Int]
shorterPath (-1)          _    _  = Nothing
shorterPath _         Nothing _   = Nothing
shorterPath numberLeft (Just map') (x,y) 
         | isDone map'               = Just []
         | otherwise                 = shortest' $ this numberLeft 1
    where
    this _    5    = []
    this max' n2   = case rec numberLeft n2 of
        (Just ans) -> (ans : this (min (length ans) max') (n2+1) )
        _          -> this max' (n2+1)
    rec max' n = case getPos (x,y) n of
        (Just nPos) -> rec' n nPos max'
        _           -> Nothing
    rec' n nPos max' = case allPaths (numberLeft-1) (updateMap map' (x,y) n) nPos of
        (Just ans)  -> Just (n:ans)
        _           -> Nothing

updateMap :: [[String]] -> (Int,Int) -> Int -> Maybe [[String]]
updateMap map (x,y) dir 
    | getPos (x,y) dir == Nothing  = Nothing
    | posval == "album" || posval == "song" || posval == "playlist" = Just $ update map (x',y') "empty"
    | posval /= "wall" = Just $ update (update map (x,y) "empty") (x',y') "monkey"
    | otherwise        = Nothing
    where
    Just (x',y') = getPos (x,y) dir
    posval = map !! y' !! x'

getPos :: (Int,Int) -> Int -> Maybe (Int,Int)
getPos (x,y) 1 | y > 0     = Just (x,y-1)
               | otherwise = Nothing
getPos (x,y) 2 | y < 5     = Just (x,y+1)
               | otherwise = Nothing
getPos (x,y) 3 | x > 0     = Just (x-1,y)
               | otherwise = Nothing
getPos (x,y) 4 | x < 5     = Just (x+1,y)
               | otherwise = Nothing

shortest :: [Maybe[Int]] -> Maybe [Int]
shortest list | list' /= [] = Just $ head [a|a<-list', length a == minimum [length b | b <- list']]
              | otherwise   = Nothing
    where list' = [ fromJust c | c <- list, c /= Nothing] 


shortest' :: [[Int]] -> Maybe [Int]
shortest' list | list' /= [] = Just $ head [a|a<-list', length a == minimum [length b | b <- list']] 
               | otherwise   = Nothing
    where list' = [ c | c <- list, c /= [] ] 

isDone :: [[String]] -> Bool
isDone list = and [ and [ okayCell a | a <- as] | as <- list ]
    where okayCell a = a == "empty" || a == "wall" || a == "monkey"  || a == "playlist" || a=="song" -- || a=="album"

--Takes a list and a tuple of a position and a new value. 
--Replaces the value of the position with the new value.
(!!=) :: [a] -> (Int,a) -> [a]
[] !!= _          = error "(!!=): List is empty"
(l:ls) !!= (0, a) = a:ls
(l:ls) !!= (i, a)
    | i < 0             = error "(!!=): Index negative" 
    | i < length (l:ls) = l: ( ls !!= ( i-1, a ) )
    | otherwise         = error "(!!=): Index larger than list"

--Given a sudoku, pos and a new value returnes a updated sudoku
update :: [[String]] -> (Int,Int) -> String -> [[String]]
update sud (x,y) val = sud !!= (y,sud!!y!!=(x,val)) 

fromJust :: Maybe a -> a
fromJust (Just a) = a
fromJust Nothing  = error "fromJust: Nothing"

main :: IO ()
main = putStr $ show $ allPaths 26 (Just example) (0,5)
