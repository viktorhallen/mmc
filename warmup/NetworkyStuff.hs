module NetworkyStuff where 

import Network.HTTP
import Network.URI
import Data.List
import Data.Maybe
import Data.Char

-- import Data.Aeson
-- import GHC.Generics

import Data.Maybe

data Command = Command 
    { command :: String
    , dir     :: String
    , apiKey  :: String
    }

instance Show Command where
    show c | command c == "new game" = 
                "{" ++ "\"apiKey\": \"" ++ apiKey c ++ "\", \"command\": \"" ++ command c ++ "\"}"
           | otherwise               = 
                "{" ++ "\"apiKey\": \"" ++ apiKey c ++ "\", \"command\": \"" ++ command c ++ "\", \"direction\":\"" ++ dir c ++ "\"}"
  

ourKey = "VtCAqjQWYHjXEa2R53pSkP3FjA8="

sendRequest :: Command -> IO String
sendRequest command = do
    a <- simpleHTTP $ commandRequest command
    getResponseBody a

new_game_command = Command
    { command   = "new game"
    , dir       = ""
    , apiKey    = ourKey
    }

teamURI = fromJust $ parseURI "http://warmup.monkeymusicchallenge.com/team/Chameleon%20Contour"

commandRequest :: Command -> Request String
commandRequest c = Request 
    { rqURI     = teamURI
    , rqMethod  = POST
    , rqHeaders = [Header HdrContentType "application/json", Header HdrContentLength (show (length (show c)))]
    , rqBody    = show c
    }

debugRequest :: Command -> Request String
debugRequest c = Request 
    { rqURI     = fromJust $ parseURI "http://requestb.in/1e3bke71"
    , rqMethod  = POST
    , rqHeaders = [Header HdrContentType "application/json", Header HdrContentLength (show (length (show c)))]
    , rqBody    = show c
    }





data GameStats = GameStats 
  { layout :: [[String]]
  , turns  :: Int
  , position :: (Int,Int)
  , pickedUp :: [String]
  , win    :: Bool
  } deriving (Show)


type Parser a = String -> Maybe (a,String)
type PO a = Maybe (a,String)

parse :: Parser GameStats
parse a = case ( parseJson "layout" (initState a) :: PO [[String]] ) of
            Just (layout', s0) -> case ( parseJson "turns" s0 :: PO Int ) of
                Just (turns', s1) -> case ( parseJson "position" s1 :: PO [Int] ) of
                    Just ([posY,posX], s2) -> case ( parseJson "pickedUp" s2 :: PO [String] ) of
                        Just (pickedUp', s3) -> case ( parseJson "win" (init s3) :: PO Bool ) of
                            Just (win', _) -> Just ( GameStats layout' turns' (posY,posX) pickedUp' win' , "")
                            _                -> Nothing
                        _                -> Nothing
                    _                -> Nothing
                _                -> Nothing
            _                -> Nothing
      

initState []                                                                 = []
initState (c:cs) | "true" `isPrefixOf` (c:cs) || "false" `isPrefixOf` (c:cs) = (toUpper c):cs
            | otherwise                                                 = c : initState cs

parseJson :: Read a => String -> Parser a
parseJson key (_:s) | isPrefixOf ("\""++key++"\":") s = case (reads (drop (length key + 3) s)) of
                                                  [(layout', s0)] -> Just (layout', s0)
                                                  _               -> Nothing
                  | otherwise                  = Nothing

exampleString = "{\"layout\":[[\"user\",\"empty\",\"empty\",\"empty\",\"empty\",\"empty\"],[\"playlist\",\"empty\",\"empty\",\"wall\",\"wall\",\"empty\"],[\"wall\",\"wall\",\"wall\",\"album\",\"empty\",\"empty\"],[\"empty\",\"empty\",\"empty\",\"wall\",\"empty\",\"song\"],[\"empty\",\"wall\",\"empty\",\"empty\",\"empty\",\"wall\"],[\"monkey\",\"wall\",\"song\",\"wall\",\"empty\",\"playlist\"]],\"turns\":30,\"position\":[5,0],\"pickedUp\":[],\"win\":false}"

--type Parser a = String -> Maybe (a, String)

{- 

Grammar:

JSON:   '{' "key": "field", .. , "key": "field" '}'
key:    layout | turns | position | pickedUp | win
field:  '[' '[' tile , .. , tile ']' , .. , '[' tile , .. , tile ']' ']'    |
        Int                                                                 |
        '[' Int , Int ']'                                                   |
        '[' tile , .. , tile ']'                                            |
        Bool
tile:   " (Empty | Wall | Song | Album | Playlist | User | Player) "

-}
{-
tile :: Parser String
tile s = undefined -- case 

num :: Parser Int
num s = listToMaybe $ reads s
-}

{-

{
"layout":
  [
    ["user","empty","empty","empty","empty","empty"],
    ["playlist","empty","empty","wall","wall","empty"],
    ["wall","wall","wall","album","empty","empty"],
    ["empty","empty","empty","wall","empty","song"],
    ["empty","wall","empty","empty","empty","wall"],
    ["monkey","wall","song","wall","empty","playlist"]
  ],
"turns":30,
"position":[5,0],
"pickedUp":[],
"win":false}

-}
