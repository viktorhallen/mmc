data Direction = UP | LEFT | RIGHT | DOWN
    deriving (Eq, Enum)

showDir UP      = "up"
showDir LEFT    = "left"
showDir RIGHT   = "right"
showDir DOWN    = "down"

instance Show Direction where
    show = showDir

data Tile =
    Empty       |
    Wall        |
    Song        |
    Album       |
    Playlist    |
    User        |
    Player
    deriving (Show, Eq)

type Map = [(Tile, Pos)]

type Path = [Direction]

type Pos = (Integer, Integer)

move :: Direction -> Pos -> Pos
move UP    (y, x)   = (y-1, x  )
move DOWN  (y, x)   = (y+1, x  )
move LEFT  (y, x)   = (y  , x-1)
move RIGHT (y, x)   = (y  , x+1)

followPath :: Path -> Pos -> Pos
followPath []     p = p
followPath (d:ds) p = followPath ds (move d p)

isOkayPath :: Path -> Pos -> Pos -> Bool
isOkayPath p s e = followPath p s == e

tile :: Map -> Pos -> Tile
tile [] _       = Wall
tile ((t, xy):tsps) p
    | xy == p   = t
    | otherwise = tile tsps p

fstPath :: [Path] -> Path
fstPath (p:ps) = p
fstPath []     = []

pathFind :: Map -> Pos -> Pos -> [Pos] -> Path
pathFind m s e prev
    | s == e    = []
    | otherwise = fstPath $ filter (\p -> isOkayPath p s e) 
                        [dir:(pathFind m (move dir s) e (s:prev)) |
                            dir <- [(UP)..(DOWN)], 
                            not ((move dir s) `elem` prev), 
                            tile m (move dir s) /= Wall]

{-
pathTree m s e prev = [dir:(pathTree m (move dir s) e (s:prev)) |
                            dir <- [(UP)..(DOWN)], 
                            not ((move dir s) `elem` prev), 
                            tile m (move dir s) /= Wall]
-}

theMap = [User    , Empty, Empty, Empty, Empty, Empty,
          Playlist, Empty, Empty, Wall , Wall , Empty,
          Wall    , Wall , Wall , Album, Empty, Empty,
          Empty   , Empty, Empty, Wall , Empty, Song ,
          Empty   , Wall , Empty, Empty, Empty, Wall ,
          Player  , Wall , Song , Wall , Empty, Playlist] `zip`
         [(y, x) | y <- [0..5], x <- [0..5]]
-- allPaths numberLeft map (x,y) = min [
-- [dir:(pathFind m (move dir s) e (s:prev)) | dir <- [(UP)..(DOWN)], not ((move dir s) `elem` prev), tile m (move dir s) /= Wall]
