module AStarMonkey where

import Data.Graph.AStar
import NetworkyStuff
import Data.Set (Set)
import qualified Data.Set as Set
import Test.QuickCheck

type Pos = (Int,Int,Map)
type Map = [[String]] 



ai :: GameStats -> String
ai stats = head $ route (layout stats) (turns stats) 

--todo: filter out paths longer than turns left
--todo: calculate the costs of all paths
route :: Map -> Int -> [String]
route map turns | allGoals == []                     = coord2dir startPos $ route2coord $ path testGoal
                | length' (path testGoal) > turns    = coord2dir startPos $ route2coord $ path testGoal
                | otherwise                          = coord2dir startPos $ route2coord $ snd $ minimum $ Prelude.map path' allGoals
    where 
    length' Nothing  = 10000
    length' (Just a) = length a
    testGoal = head $ find "user" map
    startPos = head $ find "monkey" map
    allGoals = [ (x,y,map) | x <- [0..(length (head map))-1], y <- [0..(length map)-1], isGoal (valueAt (x,y,map)) ]
    path goal = aStar neighbours dist1 (heuristicDist goal) (== goal) startPos
    path' goal = (length' (path goal), path goal)
    
    


fromJust :: Maybe a -> a
fromJust (Just a) = a

isGoal :: String -> Bool
isGoal a = any (==a) ["playlist","album","song"]

--routeAndShow :: Map -> IO ()
--routeAndShow map' = putStr . unlines . (route map' 20)


example = [
    ["user",    "empty",    "empty",    "empty",    "empty",    "empty"], 
    ["playlist","empty",    "empty",    "wall",     "wall",     "empty"], 
    ["wall",    "wall",     "wall",     "album",    "empty",    "empty"], 
    ["empty",   "empty",   "empty",    "wall",     "empty",    "song"], 
    ["empty",   "wall",     "empty",    "empty",    "empty",    "wall"], 
    ["monkey",   "wall",     "song",     "wall",     "empty",    "playlist"]]


neighbours :: Pos -> Set Pos
neighbours (x,y,map) = Set.fromList $ reverse [ (x',y',map) | (x',y') <- neibs, map !! y' !! x' /= "wall" ]
    where 
    neibs  = [ (x,y) | (x,y) <- neibs', x>=0, y>=0, x < length (head map), y < length map ]
    neibs' = [(x+1,y), (x-1,y), (x,y+1), (x,y-1)]

dist1 :: Pos -> Pos -> Int
dist1 pos _ | valueAt pos == "user" = 100
            | isGoal (valueAt pos)  = 1
            | otherwise             = 10

heuristicDist :: Pos -> Pos -> Int
heuristicDist (x',y',_) (x,y,_) = abs (x'-x) + abs (y'-y)


find :: String -> Map -> [Pos]
find what map = [(x,y,map) | x <- [0..(length (head map))-1], y <- [0..(length map)-1], map !! y !! x == what ]


valueAt :: Pos -> String
valueAt (x,y,map) = map !! y !! x


--startPos = (0,5,example) :: Pos

showRoute :: Maybe [Pos] -> String
showRoute (Just ((x,y,map):as)) = show (x,y) ++ '\n' : showRoute (Just as)
showRoute (Just [])             = ""
showRoute Nothing               = "No route found"


route2coord :: Maybe [Pos] -> [(Int,Int)]
route2coord (Just ((x,y,map):as)) = (x,y) : route2coord (Just as)
route2coord _                     = []

coord2dir :: Pos -> [(Int,Int)] -> [String]
coord2dir _       []   = []
coord2dir (x,y,_) list = zipWith pairsDir ((x,y):list) list

pairsDir :: (Int,Int) -> (Int,Int) -> String
pairsDir (x,y) (x',y') | x < x' = "right"
                       | x > x' = "left"
                       | y < y' = "down"
                       | y > y' = "up"
                       | otherwise = ""


data Map' = Map' Map
  deriving (Eq, Show)
-- cell generates an arbitrary cell in a Sudoku
cell :: Gen (String)
cell = frequency
         [(20, return "empty")
         ,(1, return "playlist")
         ,(1, return "album")
         ,(1, return "song")
         ,(2, return "wall")
         ,(1, return "user")] 

-- an instance for generating Arbitrary Sudokus
instance Arbitrary Map' where
  arbitrary =
    do a <- arbitrary :: Gen (Positive Integer)
       b <- arbitrary :: Gen (Positive Integer)
       rows <- sequence [ sequence [ cell | j <- [2..a+2] ]  | i <- [2..b+2] ]
       return (Map' rows)

prop_path :: Map' -> Property
prop_path (Map' map) = hasUser map ==> [""] /= route map 20

hasUser map' = or $ map (any (=="user")) map'
