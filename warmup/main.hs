import AStarMonkey
import NetworkyStuff


main :: IO ()
main = do
    a <- sendRequest new_game_command
    aiLoop $ parse a
    
aiLoop (Just (gameState,_)) = do
    putStrLn $ show $ turns gameState
    if (win gameState) 
    then do
        putStrLn $ "You win!!\n\nThings you have:\n" ++ unlines (pickedUp gameState)
    else if (turns gameState) < 1
    then do
        putStrLn "Out of turns. No path found :("
    else do
        let dir = ai gameState
        putStrLn dir
        a2 <- sendRequest (Command "move" dir ourKey)
        aiLoop $ parse a2

aiLoop Nothing = error "something went wrong :("


