module NetworkyStuff where 

import Network.HTTP
import Network.URI
import Data.List
import Data.Maybe
import Data.Char

-- import Data.Aeson
-- import GHC.Generics

import Data.Maybe

data Command = Command 
    { command :: String
    , dir     :: (String,String)
    , apiKey  :: String
    , team    :: String
    , gameId  :: String
    }

instance Show Command where
    show c | command c == "join game" = 
                "{" ++ "\"apiKey\": \"" ++ apiKey c  ++ 
                "\", \"command\": \""   ++ command c ++ 
                "\", \"team\": \""   ++ team c ++ 
                "\", \"gameId\": \""   ++ gameId c ++ "\"}"
           | command c == "use" = 
                "{" ++ "\"apiKey\": \"" ++ apiKey c  ++ 
                "\", \"command\": \""   ++ command c ++ 
                "\", \"item\":\""  ++ (fst (dir c))     ++
                "\", \"team\": \""   ++ team c ++ 
                "\", \"gameId\": \""   ++ gameId c ++ "\"}"
           |otherwise               = 
                "{" ++ "\"apiKey\": \"" ++ apiKey c  ++ 
                "\", \"command\": \""   ++ command c ++ 
                "\", \"directions\": [\""++ (fst (dir c)) ++ "\",\"" ++ snd (dir c) ++ "\"]"   ++
                ", \"team\": \""   ++ team c ++ 
                "\", \"gameId\": \""   ++ gameId c ++ "\"}" 
-- Add case for dirS
  

ourKey = "VtCAqjQWYHjXEa2R53pSkP3FjA8="

sendRequest :: Command -> IO String
sendRequest command = do
    a <- simpleHTTP $ commandRequest command
    getResponseBody a

new_game_command team' id = Command
    { command   = "join game"
    , dir       = ("","")
    , apiKey    = ourKey
    , team      = team'
    , gameId    = id
    }

teamURI = fromJust $ parseURI 
    "http://competition.monkeymusicchallenge.com/game/"

commandRequest :: Command -> Request String
commandRequest c = Request 
    { rqURI     = teamURI
    , rqMethod  = POST
    , rqHeaders = [Header HdrContentType "application/json", 
                  Header HdrContentLength (show (length (show c)))]
    , rqBody    = show c
    }

debugRequest :: Command -> Request String
debugRequest c = Request 
    { rqURI     = fromJust $ parseURI "http://requestb.in/1mki0ba1"
    , rqMethod  = POST
    , rqHeaders = [Header HdrContentType "application/json", 
                  Header HdrContentLength (show (length (show c)))]
    , rqBody    = show c
    }





data GameStats = GameStats 
  { layout      :: [[String]]
  , turns       :: Int
  , gameOver    :: Bool
  , score       :: Int
  , position    :: (Int,Int)
  , pickedUp    :: [String]
  , inventorySize :: Int
  , speedy      :: Int
  } deriving (Show)
data Buffs = Buffs 
  { speedyB     :: Int } deriving (Show)


type Parser a = String -> Maybe (a,String)
type PO a = Maybe (a,String)

{-
"inventorySize\":3,\"remainingTurns\":50,\"isGameOver\":false,\"buffs\":{},\"position\":[7,0],\"inventory\":[],\"score\":0}
-}

parse :: Parser GameStats
parse a = case ( parseJson "layout" (initState a) :: PO [[String]] ) of
            Just (layout', s0) -> case ( parseJson "inventorySize" s0 :: PO Int ) of
                Just (inventorySize', s1) -> case ( parseJson "remainingTurns" s1 :: PO Int ) of
                    Just (turns', s2) -> case ( parseJson "isGameOver" s2 :: PO Bool ) of
                        Just (gameOver', s3) -> case ( parseBuffs s3 ) of
                            Just (buffs', s4) -> case ( parseJson "position" (tail s4) :: PO [Int] ) of
                                Just ([posY,posX], s5) -> case ( parseJson "inventory" s5 :: PO [String] ) of
                                    Just (pickedUp', s6) -> case ( parseJson "score" s6 :: PO Int ) of
                                        Just (score', _) -> Just ( GameStats layout' turns' gameOver' score' (posY,posX) pickedUp' inventorySize' (speedyB buffs') , "")
                                        _                 -> Nothing
                                    _                -> Nothing
                                _                -> Nothing
                            _                -> Nothing
                        _                -> Nothing
                    _                -> Nothing
                _                -> Nothing
            _                -> Nothing

parseBuffs :: Parser Buffs
parseBuffs a = case ( parseJson "speedy" (drop 9 a) :: PO Int ) of
            Just (speedy', s0) -> Just (Buffs speedy', s0)
            _                  -> case ( parseJson "trapped" (drop 9 a) :: PO Int ) of
                                    Just (buff, s1) -> Just (Buffs buff, s1)
                                    _               -> Just (Buffs 0, drop 10 a) 


initState []                                                                 = []
initState (c:cs) |  "true" `isPrefixOf` (c:cs) || 
                    "false" `isPrefixOf` (c:cs)  = (toUpper c):cs
            | otherwise                                                 = c : initState cs

parseJson :: Read a => String -> Parser a
parseJson key (_:s) | isPrefixOf ("\""++key++"\":") s = case 
                        (reads (drop (length key + 3) s)) of
                            [(layout', s0)] -> Just (layout', s0)
                            _               -> Nothing
                    | otherwise                  = Nothing
{-
exampleString = "{\"layout\":[
        [\"user\",\"empty\",\"empty\",\"empty\",\"empty\",\"empty\"],
        [\"playlist\",\"empty\",\"empty\",\"wall\",\"wall\",\"empty\"],
        [\"wall\",\"wall\",\"wall\",\"album\",\"empty\",\"empty\"],
        [\"empty\",\"empty\",\"empty\",\"wall\",\"empty\",\"song\"],
        [\"empty\",\"wall\",\"empty\",\"empty\",\"empty\",\"wall\"],
        [\"monkey\",\"wall\",\"song\",\"wall\",\"empty\",\"playlist\"]
        ],\"turns\":30,\"position\":[5,0],\"pickedUp\":[],\"win\":false}"
-}
--type Parser a = String -> Maybe (a, String)

{- 

Grammar:

JSON:   '{' "key": "field", .. , "key": "field" '}'
key:    layout | turns | position | pickedUp | win
field:  '[' '[' tile , .. , tile ']' , .. , '[' tile , .. , tile ']' ']'    |
        Int                                                                 |
        '[' Int , Int ']'                                                   |
        '[' tile , .. , tile ']'                                            |
        Bool
tile:   " (Empty | Wall | Song | Album | Playlist | User | Player) "

-}
{-
tile :: Parser String
tile s = undefined -- case 

num :: Parser Int
num s = listToMaybe $ reads s
-}

{-

{
"layout":
  [
    ["user","empty","empty","empty","empty","empty"],
    ["playlist","empty","empty","wall","wall","empty"],
    ["wall","wall","wall","album","empty","empty"],
    ["empty","empty","empty","wall","empty","song"],
    ["empty","wall","empty","empty","empty","wall"],
    ["monkey","wall","song","wall","empty","playlist"]
  ],
"turns":30,
"position":[5,0],
"pickedUp":[],
"win":false}

-}
