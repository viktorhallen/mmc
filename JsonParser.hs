module JsonParser where

import Data.List
import Data.Char


exampleString = "{\"layout\":[[\"user\",\"empty\",\"empty\",\"empty\",\"empty\",\"empty\"],[\"playlist\",\"empty\",\"empty\",\"wall\",\"wall\",\"empty\"],[\"wall\",\"wall\",\"wall\",\"album\",\"empty\",\"empty\"],[\"empty\",\"empty\",\"empty\",\"wall\",\"empty\",\"song\"],[\"empty\",\"wall\",\"empty\",\"empty\",\"empty\",\"wall\"],[\"monkey\",\"wall\",\"song\",\"wall\",\"empty\",\"playlist\"]],\"turns\":30,\"position\":[5,0],\"pickedUp\":[],\"win\":false}"

a = exampleString
{-

{
"layout":
  [
    ["user","empty","empty","empty","empty","empty"],
    ["playlist","empty","empty","wall","wall","empty"],
    ["wall","wall","wall","album","empty","empty"],
    ["empty","empty","empty","wall","empty","song"],
    ["empty","wall","empty","empty","empty","wall"],
    ["monkey","wall","song","wall","empty","playlist"]
  ],
"turns":30,
"position":[5,0],
"pickedUp":[],
"win":false}

-}

data GameStats = GameStats 
  { layout :: [[String]]
  , turns  :: Int
  , position :: (Int,Int)
  , pickedUp :: [String]
  , win    :: Bool
  } deriving (Show)


type Parser a = String -> Maybe (a,String)
type PO a = Maybe (a,String)

parse :: Parser GameStats
parse a = case ( parseJson "layout" (initState a) :: PO [[String]] ) of
            Just (layout', s0) -> case ( parseJson "turns" s0 :: PO Int ) of
                Just (turns', s1) -> case ( parseJson "position" s1 :: PO [Int] ) of
                    Just ([posY,posX], s2) -> case ( parseJson "pickedUp" s2 :: PO [String] ) of
                        Just (pickedUp', s3) -> case ( parseJson "win" (init s3) :: PO Bool ) of
                            Just (win', _) -> Just ( GameStats layout' turns' (posY,posX) pickedUp' win' , "")
                            _                -> Nothing
                        _                -> Nothing
                    _                -> Nothing
                _                -> Nothing
            _                -> Nothing
      

initState []                                                                 = []
initState (c:cs) | "true" `isPrefixOf` (c:cs) || "false" `isPrefixOf` (c:cs) = (toUpper c):cs
            | otherwise                                                 = c : initState cs

parseJson :: Read a => String -> Parser a
parseJson key (_:s) | isPrefixOf ("\""++key++"\":") s = case (reads (drop (length key + 3) s)) of
                                                  [(layout', s0)] -> Just (layout', s0)
                                                  _               -> Nothing
                  | otherwise                  = Nothing


