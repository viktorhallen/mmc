module AStarMonkey where
import Data.List hiding (find)
import Data.Graph.AStar
import NetworkyStuff
import Data.Set (Set)
import qualified Data.Set as Set
import Test.QuickCheck

type Pos = (Int,Int,Map)
type Map = [[String]] 



ai :: Int -> GameStats -> (String, String, String)
ai n stats | speedy stats == 0 && hasBuffs (pickedUp stats) = ("use", head (filter (\a -> a=="banana" || a=="trap") (pickedUp stats)), "")
           | stuff1 == "" = ai (n+3) stats
           | otherwise    = ("move",stuff1,stuff2)
    where (stuff1,stuff2) = head' $ route n stats (layout stats) (turns stats) 

head' [] = ("","")
head'  [a] = (a,a)
head' (a:b:s)  = (a,b)

hasBuffs :: [String] -> Bool
hasBuffs stuff = or $ map (\a -> any (==a) ["banana","trap"]) stuff 

--todo: filter out paths longer than turns left
--todo: calculate the costs of all paths
route :: Int -> GameStats -> Map -> Int -> [String]
route howFar stats map turns 
    | length' (path testGoal) >= turns -1 = coord2dir startPos $ route2coord $ path testGoal
    | allGoals == [] && length (pickedUp stats) == 0 = coord2dir startPos $ route2coord $ path $ head $ filter (/= startPos) (find "monkey" map)
    | length (pickedUp stats) == (inventorySize stats)  = coord2dir startPos $ route2coord $ path testGoal
    | p' howFar == [] && (find "lever" map /= [] ) = coord2dir startPos $ route2coord $ path $ head $ find "lever" map
    | p' howFar == [] && length (pickedUp stats) == 0 = coord2dir startPos $ route2coord $ path $ head $ filter (/= startPos) (find "monkey" map)
    | p' howFar == []      = coord2dir startPos $ route2coord $ path testGoal
    | otherwise            = coord2dir startPos $ route2coord $ snd $ minimum $ p' howFar
    where 
    length' Nothing  = 10000
    length' (Just a) = length a
    testGoal = head $ filterDist 1 startPos $ find "user" map
    (startY,startX) = position stats
    startPos = (startX, startY, map)
    allGoals = [ (x,y,map) | x <- [0..(length (head map))-1], y <- [0..(length map)-1], isGoal (valueAt (x,y,map)) ]
    path goal = aStar (neighbours stats goal) cost (heuristicDist goal) (== goal) startPos
    path' goal = (length' (path goal), path goal)
    p' howFar' | howFar' > (length allGoals + 10) = []
               | p howFar' == [] = p' (howFar'+3)
               | otherwise       = p howFar'
    p howFar' = Prelude.map path' (filterDist howFar' startPos allGoals)
    
    


fromJust :: Maybe a -> a
fromJust (Just a) = a

isGoal :: String -> Bool
isGoal a = any (==a) ["playlist","album","song","banana"]
isItem :: String -> Bool
isItem a = isGoal a || any (==a) ["banana","trap"]

--routeAndShow :: Map -> IO ()
--routeAndShow map' = putStr . unlines . (route map' 20)


example = [
    ["user",    "empty",    "empty",    "empty",    "empty",    "empty"], 
    ["playlist","empty",    "empty",    "wall",     "wall",     "empty"], 
    ["wall",    "wall",     "wall",     "album",    "empty",    "empty"], 
    ["empty",   "empty",   "empty",    "wall",     "empty",    "song"], 
    ["empty",   "wall",     "empty",    "empty",    "empty",    "wall"], 
    ["monkey",   "wall",     "song",     "wall",     "empty",    "playlist"]]


neighbours :: GameStats -> Pos -> Pos -> Set Pos
neighbours gs goal (x,y,map) | valueAt goal == "user" = Set.fromList $ Prelude.map lever [ (x',y',map) | (x',y') <- neibs, isOk  (map !! y' !! x') ]
                             | otherwise              = Set.fromList $ Prelude.map lever [ (x',y',map) | (x',y') <- neibs, isOkUser (map !! y' !! x') ]
    where 
    neibs  = [ (x,y) | (x,y) <- neibs', x>=0, y>=0, x < length (head map), y < length map ]
    neibs' = [(x+1,y), (x-1,y), (x,y+1), (x,y-1)]
    isOk a = a /= "wall" && a /= "closed-door" && not (length (pickedUp gs) == (inventorySize gs) && isItem a)
    isOkUser a = isOk a && a /= "user"
    
    lever (x,y,map) | valueAt (x,y,map) == "lever" = (x,y,switchDoors map)
                    | otherwise                    = (x,y,map)
    switchDoors m = [[ if x == "open-door" then "closed-door" else if x == "closed-door" then "open-door" else x | x <- row] | row <- m ]

cost :: Pos -> Pos -> Int
cost _ pos | valueAt pos == "user"          = 1000
           | valueAt pos == "playlist"      = 2
           | valueAt pos == "album"         = 15
           | valueAt pos == "song"          = 20
           | valueAt pos == "closed-door"   = 1000
           | valueAt pos == "open-door"     = 25
           | valueAt pos == "lever"         = 100
           | isGoal (valueAt pos)           = 20
           | otherwise                      = 20

heuristicDist :: Pos -> Pos -> Int
heuristicDist (x',y',_) (x,y,_) = abs (x'-x) + abs (y'-y) - 1

filterDist :: Int -> Pos -> [Pos] -> [Pos]
filterDist n mPos pos = drop (n-3) $ take n (Data.List.sortBy sort' pos)
    where 
    sort' :: Pos -> Pos -> Ordering
    sort' p1 p2 | (heuristicDist mPos p1) < (heuristicDist mPos p2) = LT
                     | otherwise = GT

find :: String -> Map -> [Pos]
find what map = [(x,y,map) | x <- [0..(length (head map))-1], y <- [0..(length map)-1], map !! y !! x == what ]


valueAt :: Pos -> String
valueAt (x,y,map) = map !! y !! x


--startPos = (0,5,example) :: Pos

showRoute :: Maybe [Pos] -> String
showRoute (Just ((x,y,map):as)) = show (x,y) ++ '\n' : showRoute (Just as)
showRoute (Just [])             = ""
showRoute Nothing               = "No route found"


route2coord :: Maybe [Pos] -> [(Int,Int)]
route2coord (Just ((x,y,map):as)) = (x,y) : route2coord (Just as)
route2coord _                     = []

coord2dir :: Pos -> [(Int,Int)] -> [String]
coord2dir _       []   = []
coord2dir (x,y,_) list = zipWith pairsDir ((x,y):list) list

pairsDir :: (Int,Int) -> (Int,Int) -> String
pairsDir (x,y) (x',y') | x < x' = "right"
                       | x > x' = "left"
                       | y < y' = "down"
                       | y > y' = "up"
                       | otherwise = ""

{-
data Map' = Map' Map
  deriving (Eq, Show)
-- cell generates an arbitrary cell in a Sudoku
cell :: Gen (String)
cell = frequency
         [(20, return "empty")
         ,(1, return "playlist")
         ,(1, return "album")
         ,(1, return "song")
         ,(2, return "wall")
         ,(1, return "user")] 

-- an instance for generating Arbitrary Sudokus
instance Arbitrary Map' where
  arbitrary =
    do a <- arbitrary :: Gen (Positive Integer)
       b <- arbitrary :: Gen (Positive Integer)
       rows <- sequence [ sequence [ cell | j <- [2..a+2] ]  | i <- [2..b+2] ]
       return (Map' rows)
-}
--prop_path :: Map' -> Property
--prop_path (Map' map) = hasUser map ==> [""] /= route map 20

--hasUser map' = or $ map (any (=="user")) map'
