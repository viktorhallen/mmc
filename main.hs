import AStarMonkey
import NetworkyStuff
import System.Timeout
import Control.Exception
import System.Random
import System.CPUTime

gameId'' = "296"
teamName = "Chameleon Contour"

main :: IO ()
main = do
    a <- getLine
    main' a

main' :: String -> IO ()
main' gameId' = do
    a <- sendRequest $ new_game_command teamName gameId'
    putStr (show (parse a))
    aiLoop [""] gameId' $ parse a
    
aiLoop lastDir id (Just (gameState,_)) = do
    putStr $ show ( turns gameState ) ++ " "
    if (gameOver gameState) 
    then do
        putStrLn $ "Game over!\n\nThings you have:\n" ++ unlines (pickedUp gameState)
    else if (turns gameState) < 1
    then do
        putStrLn "Out of turns. No path found :("
    else do
        res <- timeout 5000000 $ evaluate $ ai 3 gameState
        let (cmd, tdir1, tdir2) = noFilter res
        i <- getStdRandom (randomR (0,2))
        let rdMove = indexNotLast lastDir i
        let (dir1,dir2) = if tdir1 == "gen" then (rdMove,rdMove) else (tdir1,tdir2)
        putStr $ "(" ++ dir1 ++ "," ++ dir2 ++ ")" 
        a2 <- sendRequest (Command cmd (dir1,dir2) ourKey teamName id)
        putStrLn a2
        aiLoop (dir2:dir1:lastDir) id $ parse a2

aiLoop _ _ Nothing = error "something went wrong :("


noFilter :: Maybe (String, String, String) -> (String, String, String)
noFilter Nothing  = ("move", "gen","gen")
noFilter (Just a) = a


indexNotLast  :: [String] -> Int -> String
indexNotLast a i | length a < 5                       = indexNotLast' (head a) i
                 | take 5 a == replicate 5 (head a)            = indexNotLast' (otherWay (head a)) i
                 | otherwise                          = indexNotLast' (head a) i
indexNotLast' :: String -> Int -> String
indexNotLast' last i = (filter (/= otherWay last) ["up","down","left","right"]) !! i

otherWay :: String -> String
otherWay a | a == "up"   = "down"
           | a == "down" = "up"  
           | a == "left" = "right"  
           | a == "right"= "left"
           | otherwise   = a   


